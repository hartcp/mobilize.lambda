﻿using System.Collections.Generic;
using System.Net.Http;

namespace Solu.Aws.Lambda.Mobilize.Model
{
    /// <summary>
    /// Return object for the lambda execution.
    /// </summary>
    public class LambdaResult
    {
        /// <summary>
        /// Gets or sets the result.
        /// </summary>
        /// <value>
        /// The result.
        /// </value>
        public HttpResponseMessage Result { get; set; }

        /// <summary>
        /// Gets or sets the result logs.
        /// </summary>
        /// <value>
        /// The result logs.
        /// </value>
        public List<string> ResultLogs { get; set; }
    }
}
