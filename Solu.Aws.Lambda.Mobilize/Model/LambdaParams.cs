﻿namespace Solu.Aws.Lambda.Mobilize.Model
{
    /// <summary>
    /// The parameters being passing into the lambda function.
    /// </summary>
    public class LambdaParams
    {
        /// <summary>
        /// Gets or sets the API method to invoke.
        /// </summary>
        /// <value>
        /// The API method to invoke.
        /// </value>
        public string ApiMethod { get; set; }

        /// <summary>
        /// Gets or sets the target URL to reference.
        /// </summary>
        /// <value>
        /// The target URL to reference.
        /// </value>
        public string TargetUrl { get; set; }
    }
}