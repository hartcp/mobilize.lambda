﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Solu.Aws.Lambda.Mobilize.Model
{
    class TokenModel
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string userName { get; set; }
        public string userId { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string organizationId { get; set; }

    }
}
