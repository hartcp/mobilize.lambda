﻿using System;
using System.Text;

namespace Solu.Aws.Lambda.Mobilize.Utility
{
    /// <summary>
    /// Helper class to get error messages to be logged.
    /// </summary>
    internal static class ErrorHelper
    {
        /// <summary>
        /// Generates the error message entry.
        /// </summary>
        /// <param name="exception">The exception.</param>
        /// <returns>The error message.</returns>
        internal static string GetErrorMessage(Exception exception)
        {
            var msg = new StringBuilder("0: ");

            msg.Append(exception.Message);

            var ex = exception.InnerException;

            var i = 0;
            while (ex != null)
            {
                msg.AppendFormat("\n\t{0}: {1}", ++i, ex.Message);
                ex = ex.InnerException;
            }

            return msg.ToString();
        }
    }
}
