﻿using System.IO;
using System.Text;

using Amazon.Lambda.Serialization.Json;

namespace Solu.Aws.Lambda.Mobilize.Utility
{
    /// <summary>
    /// JSON Helper class used to serialize an object into a JSON string.
    /// </summary>
    internal static class JsonHelper
    {
        private static readonly JsonSerializer JsonSerializer = new JsonSerializer();

        /// <summary>
        /// Serializes the object.
        /// </summary>
        /// <param name="streamRecord">The stream record.</param>
        /// <returns>The JSON serialized string.</returns>
        internal static string SerializeObject(object streamRecord)
        {
            using (var ms = new MemoryStream())
            {
                JsonSerializer.Serialize(streamRecord, ms);

                return Encoding.UTF8.GetString(ms.ToArray());
            }
        }
    }
}
