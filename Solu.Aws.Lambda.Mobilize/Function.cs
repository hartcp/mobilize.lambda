using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Solu.Aws.Lambda.Mobilize.Model;
using Solu.Aws.Lambda.Mobilize.Utility;
using System.Text;
using System.Net.Http.Headers;
using Newtonsoft.Json;

//using Solu.Aws.Lambda.Cakes.Model;
//using Solu.Aws.Lambda.Cakes.Utility;
//using System.Configuration;
//using Solu.DevOps.Common.Utility;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace Solu.Aws.Lambda.Mobilize
{
    public class Function
    {
        /// <summary>
        /// A simple function that takes 2 parameters that are key/value pairs. Then performs the data refresh for the given API mehtod and target URL.
        /// </summary>
        /// <param name="lambdaParams">The lambda parameters being passed in.</param>
        /// <param name="context">The lambda context.</param>
        /// <returns>JSON string indicating success or failure, with the log results of the execution.</returns>
        public async Task<string> FunctionHandler(LambdaParams lambdaParams, ILambdaContext context)
        {
            var stopwatch = new Stopwatch();
            var resultLogs = new List<string>();
            try
            {
                /****************************************************
                 * Development: http://ec2-34-193-42-136.compute-1.amazonaws.com/MobilizeServices
                 * Production: 
                 */
                var targetUrl = lambdaParams.TargetUrl;

                var targetUrlLog = string.Format("Target URL -- {0}", targetUrl);
                context.Logger.Log(targetUrlLog);
                resultLogs.Add(targetUrlLog);

                
                var apiMethod = string.Format("{0}", lambdaParams.ApiMethod);

                var apiMethodLog = string.Format("API Method -- {0}", apiMethod);
                context.Logger.Log(apiMethodLog);
                resultLogs.Add(apiMethodLog);



                var UserName = Environment.GetEnvironmentVariable("USER");
                var Password = Environment.GetEnvironmentVariable("PASSWORD");

                var token = await GetToken(UserName, Password, targetUrl);

                if (String.IsNullOrWhiteSpace(token))
                {
                    context.Logger.Log(string.Format("Execution error for function -- {0} - A problem occurred acquiring authorization", context.FunctionName));                    
                    return HttpStatusCode.InternalServerError.ToString();
                }

                using (var httpClient = new HttpClient())
                {
                    httpClient.BaseAddress = new Uri(targetUrl);

                    stopwatch.Start();

                    var beginLog = string.Format("Beginning execution for function -- {0} at {1}", context.FunctionName,
                        DateTime.Now);
                    context.Logger.Log(beginLog);

                    resultLogs.Add(beginLog);

                    httpClient.Timeout = new TimeSpan(0, 5, 0);

                    httpClient.DefaultRequestHeaders.UserAgent.Clear();
                    httpClient.DefaultRequestHeaders.UserAgent.ParseAdd("Chrome/22.0.1229.94");
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);

                    return await httpClient.GetAsync(apiMethod).ContinueWith(responseTask =>
                    {
                        var result = responseTask.Result;

                        stopwatch.Stop();

                        var logResult = string.Format("Execution complete for function -- {0} at {1} duration {2}",
                            context.FunctionName, DateTime.Now, stopwatch.Elapsed);

                        context.Logger.Log(logResult);

                        var requestMessageLog = string.Format("Execution Result -- {0}", result.RequestMessage);
                        var statusCodeLog = string.Format("Execution Status -- {0}", result.StatusCode);

                        context.Logger.Log(requestMessageLog);
                        context.Logger.Log(statusCodeLog);

                        resultLogs.Add(logResult);
                        resultLogs.Add(requestMessageLog);
                        resultLogs.Add(statusCodeLog);


                        return JsonHelper.SerializeObject(new
                        {
                            Result = new LambdaResult
                            {
                                Result = result,
                                ResultLogs = resultLogs
                            },
                            HttpStatusCode = responseTask.Status
                        });
                    });
                }
            }
            catch (HttpRequestException hre)
            {
                stopwatch.Stop();

                context.Logger.Log(string.Format("Execution error for function -- {0} at {1} duration {2}", context.FunctionName, DateTime.Now, stopwatch.Elapsed));

                context.Logger.Log(ErrorHelper.GetErrorMessage(hre));
            }
            catch (TaskCanceledException tce)
            {
                stopwatch.Stop();

                context.Logger.Log(string.Format("Execution error for function -- {0} at {1} duration {2}", context.FunctionName, DateTime.Now, stopwatch.Elapsed));

                context.Logger.Log(ErrorHelper.GetErrorMessage(tce));
            }
            catch (Exception ex)
            {
                stopwatch.Stop();

                context.Logger.Log(string.Format("Execution error for function -- {0} at {1} duration {2}", context.FunctionName, DateTime.Now, stopwatch.Elapsed));

                context.Logger.Log(ErrorHelper.GetErrorMessage(ex));

                return HttpStatusCode.InternalServerError.ToString();
            }

            return HttpStatusCode.BadRequest.ToString();
        }


        public async Task<string> GetToken(string userName, string password, string baseUrl)
        {
            var pairs = new List<KeyValuePair<string, string>>
                        {
                            new KeyValuePair<string, string>( "grant_type", "password" ),
                            new KeyValuePair<string, string>( "username", userName ),
                            new KeyValuePair<string, string> ( "Password", password )
                        };
            var content = new FormUrlEncodedContent(pairs);
            using (var client = new HttpClient())
            {
                var fullUrl = String.Format("{0}/MobilizeServices/Token", baseUrl);
                
                var response = await client.PostAsync(fullUrl, content).ContinueWith(responseTask =>
                {
                    return responseTask.Result;
                });                

                var tokenModel = JsonConvert.DeserializeObject<TokenModel>(response.Content.ReadAsStringAsync().Result);

                return tokenModel.access_token;
            }
        }
    }
}
